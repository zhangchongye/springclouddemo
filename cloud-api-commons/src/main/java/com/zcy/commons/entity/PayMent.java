package com.zcy.commons.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data  //不用写Getter,Setter,equals,canEqual,hasCode,toString
@AllArgsConstructor  //使用后添加一个构造函数，含所有参数
@NoArgsConstructor   //使用后添加一个构造函数，不含参数
public class PayMent implements Serializable {
//    什么情况下需要序列化：
//    1.     当你想把的内存中的对象写入到硬盘的时候。
//    2.     当你想用套接字在网络上传送对象的时候。
//    3.     当你想通过RMI传输对象的时候。
    private Long id;
    private String serial;


}
