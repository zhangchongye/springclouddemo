package com.zcy.order.controller;
import com.zcy.commons.entity.CommonResult;
import com.zcy.commons.entity.PayMent;
import com.zcy.order.lb.LoadBalance;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.net.URI;
import java.util.List;

@RestController
@RequestMapping("/order")
@Slf4j
public class OrderController {
    @Autowired
    private RestTemplate restTemplate;
    @Autowired
    private LoadBalance loadBalance;
    @Autowired
    private DiscoveryClient discoveryClient;
//    private String baseUrl="http://localhost:8001/payMent";
    private String baseUrl="http://CLOUD-PAYMENT-SERVICE/payMent";

    @PostMapping("/savePayMent")
    public CommonResult<PayMent> savePayMent(PayMent payMent){
        log.info("payMent=={}",payMent);
        return restTemplate.postForObject(baseUrl+"/createPayMent",payMent,CommonResult.class);
    }

    @GetMapping("/getPayMent/{id}")
    public CommonResult<PayMent> getPayMent(@PathVariable("id")Long id){
        log.info("getPayMent,id={}",id);
        log.info("getPayMent--fffffff,id={}",id);
        log.info("getPayMeaaaaa,id={}",id);
        return  restTemplate.getForObject(baseUrl+"/get/"+id,CommonResult.class);
    }



    @GetMapping("/getPayMentEntity/{id}")
    public CommonResult<PayMent> getPayMent2(@PathVariable("id") Long id){
        ResponseEntity<CommonResult> entity = restTemplate.getForEntity(baseUrl + "/get/" + id, CommonResult.class);
        if(entity.getStatusCode().is2xxSuccessful()){
            return entity.getBody();
        }else{
            return new CommonResult<>(444,"查询失败");
        }

    }

    @GetMapping("/getPayMentByMyLb")
    public String  getPayMentByMyLb(){
        log.info("getPayMentByMyLb");

        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        if(instances==null||instances.size()==0){
            return  null;
        }
        ServiceInstance instance = loadBalance.instances(instances);
        URI uri = instance.getUri();
        return "123456";
//        return  restTemplate.getForObject(uri+"/payMent/getPayMentLb",String.class);
    }
}
