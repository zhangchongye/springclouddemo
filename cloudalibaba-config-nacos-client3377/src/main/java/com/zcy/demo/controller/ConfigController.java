package com.zcy.demo.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: zcy
 * @Date: 2020/07/16/2:13
 * @Description:
 */
@RestController
@RefreshScope
public class ConfigController {
    @Value("${server.port}")
    private String serverPort;
    @Value("${config.info}")
    private String configInfo;
    @GetMapping("/justDemo")
    public String justDemo(){
        System.out.println("serverPort==="+serverPort);
        System.out.println("configInfo==="+configInfo);
        return  configInfo;
    }
}
