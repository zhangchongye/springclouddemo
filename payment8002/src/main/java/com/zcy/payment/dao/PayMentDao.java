package com.zcy.payment.dao;

import com.zcy.commons.entity.PayMent;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface PayMentDao {
    public int createPayMent(PayMent payMent);
    public PayMent getPayMentById(@Param("id") Long id);
}
