package com.zcy.payment.service.impl;

import com.zcy.commons.entity.PayMent;
import com.zcy.payment.dao.PayMentDao;
import com.zcy.payment.service.PayMentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PayMentServiceImpl implements PayMentService {
    @Autowired
    private PayMentDao payMentDao;
    //autowired通过byType通过类型来注入
    //resource通过name来注入
    @Override
    public int createPayMent(PayMent payMent) {
        return payMentDao.createPayMent(payMent);
    }

    @Override
    public PayMent getPayMentById(Long id) {
        return payMentDao.getPayMentById(id);
    }
}
