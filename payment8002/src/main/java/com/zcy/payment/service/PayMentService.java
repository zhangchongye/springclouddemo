package com.zcy.payment.service;

import com.zcy.commons.entity.PayMent;

public interface PayMentService {
    public int createPayMent(PayMent payMent);
    public PayMent getPayMentById(Long id);
}
