package com.zcy.order.service;

import com.zcy.commons.entity.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import com.zcy.commons.entity.PayMent;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface OrderService {
    @GetMapping(value = "/payMent/get/{id}")
    public CommonResult<PayMent> getPaymentById(@PathVariable("id") Long id);
}
