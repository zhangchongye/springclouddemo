package com.zcy.order.controller;


import com.netflix.discovery.converters.Auto;
import com.zcy.commons.entity.CommonResult;
import com.zcy.commons.entity.PayMent;
import com.zcy.order.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/feignOrder")
@Slf4j
public class OrderController {
    @Autowired
    private OrderService orderService;
    @GetMapping("/getPayMent/{id}")
    public CommonResult<PayMent> getPayMent(@PathVariable("id")Long id){
        log.info("getPayMent,id={}",id);
        log.info("getPayMent--fffffff,id={}",id);
        log.info("getPayMeaaaaa,id={}",id);
        return  orderService.getPaymentById(id);
    }
}
