package com.zcy.hystricPayment.controller;

import com.zcy.hystricPayment.service.PayMentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/payment")
@Slf4j
public class PaymentController {
    @Autowired
    private PayMentService payMentService;
    @Value("${server.port}")
    private String serverPort;
    @RequestMapping("/hystricOk/{id}")
    public String runOk(@PathVariable(value = "id") Long id){
        return payMentService.payment_ok(id);
    }
    @RequestMapping("/hystricTimeOut/{id}")
    public String runTimeOut(@PathVariable(value = "id") Long id){
        return payMentService.payment_timeOut(id);
    }
    @RequestMapping("/hystricCircuitBreak/{id}")
    public String hystricCircuitBreak(@PathVariable("id")Integer id){
        return  payMentService.paymentCircuitBreaker(id);
    }


}
