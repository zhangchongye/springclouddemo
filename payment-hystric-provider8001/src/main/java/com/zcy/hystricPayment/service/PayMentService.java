package com.zcy.hystricPayment.service;

import org.springframework.web.bind.annotation.PathVariable;

public interface PayMentService {
    public String payment_ok(Long id);
    public String payment_timeOut(Long id);
    public String paymentCircuitBreaker(@PathVariable("id") Integer id);
}
