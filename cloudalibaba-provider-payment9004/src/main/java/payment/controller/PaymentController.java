package com.zcy.payment.controller;

import com.zcy.commons.entity.CommonResult;
import com.zcy.commons.entity.PayMent;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: zcy
 * @Date: 2020/07/18/0:05
 * @Description:
 */
@RestController
public class PaymentController {
    @Value("${server.port}")
    private String serverPort;

    public static HashMap<Long,PayMent> hashMap = new HashMap<>();
    static
    {
        hashMap.put(1L,new PayMent(1L,"28a8c1e3bc2742d8848569891fb42181"));
        hashMap.put(2L,new PayMent(2L,"bba8c1e3bc2742d8848569891ac32182"));
        hashMap.put(3L,new PayMent(3L,"6ua8c1e3bc2742d8848569891xt92183"));
    }

    @GetMapping("/justDemo/{id}")
    public CommonResult<PayMent> justDemo(@PathVariable("id")Long id ){
        PayMent payMent = hashMap.get(id);
        return new CommonResult(200,"返回成功,port==="+serverPort,payMent);
    }
}
