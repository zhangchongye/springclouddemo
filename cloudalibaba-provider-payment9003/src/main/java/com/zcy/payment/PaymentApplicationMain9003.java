package com.zcy.payment;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * Created with IntelliJ IDEA.
 *
 * @Auther: zcy
 * @Date: 2020/07/18/0:04
 * @Description:
 */
@SpringBootApplication
@EnableDiscoveryClient
public class PaymentApplication {
    
}
