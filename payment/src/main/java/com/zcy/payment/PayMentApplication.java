package com.zcy.payment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class PayMentApplication {
    public static void main(String[] args) {
        SpringApplication.run(PayMentApplication.class,args);
    }
}
