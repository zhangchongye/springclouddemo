package com.zcy.payment.controller;


import com.zcy.commons.entity.CommonResult;
import com.zcy.commons.entity.PayMent;
import com.zcy.payment.service.PayMentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/payMent")
@Slf4j
public class PayMentController {
    @Autowired
    private PayMentService payMentService;
    @Value("${server.port}")
    private String serverPort;
    @PostMapping("/createPayMent")
    public CommonResult<Integer> createPayMent(@RequestBody PayMent payMent){
        int result = payMentService.createPayMent(payMent);
        log.info("result==="+result);
        if(result>0){
            return new CommonResult<>(200,"插入成功，端口号："+serverPort,result);
        }else{
            return new CommonResult<Integer>(500,"插入失败",null);
        }
    }
    @GetMapping("/justDemo")
    public  String justDemo(){
        return "success";
    }
    @GetMapping("/get/{id}")
    public CommonResult<PayMent> getPayMentById(@PathVariable(value="id") Long id){
        PayMent payMent = payMentService.getPayMentById(id);
        log.info("id===0.0===="+id);
        if(payMent!=null){
            return new CommonResult<PayMent>(200,"查询成功，{},端口号："+serverPort,payMent);
        }else{
            return new CommonResult<PayMent>(500,"查询失败",null);
        }
    }
    @GetMapping("/getPayMentLb")
    public String getPayMentLb(){
        return  serverPort;
    }
}
