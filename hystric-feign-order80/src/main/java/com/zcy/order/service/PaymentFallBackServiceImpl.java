package com.zcy.order.service;

import org.springframework.stereotype.Component;

@Component
public class PaymentFallBackServiceImpl implements  HystricService{
    @Override
    public String runOk(Long id) {
        return "PaymentFallBackServiceImpl-- runOk";
    }

    @Override
    public String runTimeOut(Long id) {
        return "PaymentFallBackServiceImpl-- runTimeOut";
    }
}
