package com.zcy.order.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@FeignClient(value = "CLOUD-PROVIDER-HYSTRIX-PAYMENT",fallback =PaymentFallBackServiceImpl.class)
@Component
public interface HystricService {
    @RequestMapping("payment/hystricOk/{id}")
    public String runOk(@PathVariable(value = "id") Long id);
    @RequestMapping("payment/hystricTimeOut/{id}")
    public String runTimeOut(@PathVariable(value = "id") Long id);
}
