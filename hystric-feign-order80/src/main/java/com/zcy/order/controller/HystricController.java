package com.zcy.order.controller;

import com.netflix.hystrix.contrib.javanica.annotation.DefaultProperties;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;
import com.zcy.order.service.HystricService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
@RequestMapping("/order")
@DefaultProperties(defaultFallback = "payment_Global_FallbackMethod")
public class HystricController {
    @Autowired
    public HystricService hystricService;
    @RequestMapping("/hystricOk/{id}")
    public String runOk(@PathVariable(value = "id") Long id){
        return hystricService.runOk(id);
    }
    @RequestMapping("/hystricTimeOut/{id}")
//    @HystrixCommand(fallbackMethod = "paymentTimeOutFallbackMethod",commandProperties = {
//            @HystrixProperty(name="execution.isolation.thread.timeoutInMilliseconds",value="1500")
//    })
    @HystrixCommand
    public String runTimeOut(@PathVariable(value = "id") Long id){
        return  hystricService.runTimeOut(id);
    }
    public String paymentTimeOutFallbackMethod(@PathVariable("id") Long id)
    {
        return "我是消费者80,对方支付系统繁忙请10秒钟后再试或者自己运行出错请检查自己,o(╥﹏╥)o";
    }
    public String payment_Global_FallbackMethod(){
        return "这是全局异常处理";
    }

}
